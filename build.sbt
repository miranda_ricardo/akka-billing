lazy val commonSettings = Seq(
  version := "1.0",
  scalaVersion := "2.12.3",
  organization := "com.ricardomiranda"
)
lazy val dependencies = Seq(
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-actor-typed
  "com.typesafe.akka" %% "akka-actor-typed" % "2.5.22",
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-slf4j
  "com.typesafe.akka" %% "akka-slf4j" % "2.5.22",
  // https://mvnrepository.com/artifact/ch.qos.logback/logback-classic
  "ch.qos.logback" % "logback-classic" % "1.2.3" % Test,
  // https://mvnrepository.com/artifact/org.scalatest/scalatest
  "org.scalatest" %% "scalatest" % "3.0.5" % Test,
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-testkit-typed
  "com.typesafe.akka" %% "akka-actor-testkit-typed" % "2.5.22" % Test,
  //https://mvnrepository.com/artifact/com.github.stefanbirkner/system-rules
  "com.github.stefanbirkner" % "system-rules" % "1.17.2" % Test
)
lazy val root = (project in file(".")).
  settings(
    commonSettings,
    // set the main class for packaging the main jar
    mainClass in (Compile, packageBin) := Some("com.ricardomiranda.phone.Main"),
    name := "disco-test-phone-company",
    libraryDependencies ++= dependencies
  )
assemblyJarName in assembly := s"disco-company.jar"
