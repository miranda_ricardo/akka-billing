package com.ricardomiranda.phone

import akka.actor.typed.ActorSystem
import akka.NotUsed

object Main extends App {
  println("Akka Billing program starting")

  val input: String = readLine("What is the name of the log file?\n").trim

  val system: ActorSystem[NotUsed] = ActorSystem(LogProcessor.root(input), "AkkaBilling")
  system.terminate
}
