package com.ricardomiranda.phone

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, Terminated}
import akka.NotUsed
import scala.util.Random
import scala.annotation.tailrec
import scala.io.Source
import java.nio.file.{Paths, Files}

object LogProcessor {
  val root: String => Behavior[NotUsed] = 
            input  => Behaviors.setup { ctx =>

    val dispatcher: ActorRef[Message] = ctx.spawn(new Dispatcher().rest, "Dispatcher")
    dispatcher ! Working

    Files.exists(Paths.get(input)) && input.length() > 0 match {
      case true =>
        println(s"""Log file ${input} is going to be processed""")
        for (line <- Source.fromFile(input).getLines) {
          val xs:Array[String] = line.split(' ')
          if (xs.size == 3) { dispatcher ! NewLogLine(client = xs(0), number = xs(1), duration = xs(2)) }
        }

        println(s"""Log file processing terminated""")
        dispatcher ! AskTotals
      case false =>
        println(s"""File does not exist""")
    }

    Thread.sleep(3000)
    dispatcher ! Terminate

    Behaviors.receiveSignal {
      case (_, Terminated(ref)) => Behaviors.stopped
    }
  }
}

class Dispatcher {
  case class State(clients: Map[String, ActorRef[Message]] = Map(), clientsTotals: List[(String, Double)] = List())
  
  val rest: Behavior[Message] = resting()

  private def resting(): Behavior[Message] =
    Behaviors.receive[Message] { (ctx, msg) =>
      msg match {
        case Working =>
          working(state = State())
        case _ =>
          Behaviors.same
      }
    }

  private def working(state: State): Behavior[Message] =
    Behaviors.receive[Message] { (ctx, msg) =>
      def addClient(state: State, client: String): State = state.clients.get(client) match {
        case Some(_) =>
          state
        case None =>
          val clientActor: ActorRef[Message] = ctx.spawn(new Client().rest, client)
          clientActor ! Working
          state.copy(clients = state.clients ++ Map(client -> clientActor))
      }

      msg match {
        case NewLogLine(client, number, duration) =>
          val newState: State = addClient(state = state, client = client)
          newState.clients(client) ! AddCall(number = number, duration = duration)
          working(state = newState)
        case AskTotals =>
          println(s"""Customer / Total Cost of Calls""")
          state.clients.values.toList.foreach{
            case x: ActorRef[Message] => clientTotal(client = x, self = ctx.self.ref)}
          Behaviors.same
        case ReplyTotalPounds(client, total) =>
          println(client ++ " £" ++ f"${total}%6.4f".trim)
          Behaviors.same
        case Testing =>
          testing(state = state)
        case Terminate =>
          Behaviors.stopped
        case _ =>
          Behaviors.same
      }
  }
  
  private def testing(state: State): Behavior[Message] =
    Behaviors.receive[Message] { (ctx, msg) =>
      msg match {
        case AskListOfClients(from) =>
          from ! ReplyListOfString(state.clients.keys.toList)
          Behaviors.same
        case AskTotals =>
          state.clients.values.toList.foreach{
            case x: ActorRef[Message] => clientTotal(client = x, self = ctx.self.ref)}
          Behaviors.same
        case ReplyTotalPounds(client, total) =>
          val newState: State = state.copy(clientsTotals = (client, total) :: state.clientsTotals)
          testing(state = newState)
        case SeeTotals(from) =>
          from ! ReplySetOfTuples(state.clientsTotals.toSet)
          Behaviors.same
        case _ =>
          Behaviors.same
      }
    }
  
  private def clientTotal(client: ActorRef[Message], self: ActorRef[Message]): Unit = {
    client ! AskTotal(from = self)
  }
}