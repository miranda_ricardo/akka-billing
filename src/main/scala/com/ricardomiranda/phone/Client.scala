package com.ricardomiranda.phone

import scala.annotation.tailrec

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}

class Client {
  case class State(client: String, callsCost: Map[String, Long] = Map(), maxCost: Long = 0)

  val rest: Behavior[Message] = resting()

  private def resting(): Behavior[Message] =
    Behaviors.receive[Message] { (ctx, msg) =>
      msg match {
        case Working =>
          val state: State = State(client = ctx.self.path.toString.split("/").last)
          working(state = state)
        case Testing =>
          testing()
        case _ =>
          Behaviors.same
      }
    }

  private def working(state: State): Behavior[Message] =
    Behaviors.receive[Message] { (ctx, msg) =>
      msg match {
        case AddCall(number, duration) =>
          val newState = addCall(state = state, number = number, duration = duration)
          working(state = newState)
        case AskTotal(from) =>
          from ! ReplyTotalPounds(
            client = state.client,
            total = computeTotalCostHundredthPences(
              callsCost = state.callsCost, 
              maxCost = state.maxCost) / 100.0 / 100.0)
          Behaviors.same
        case _ =>
          Behaviors.same
      }
    }
  
  private def testing(): Behavior[Message] =
    Behaviors.receive[Message] { (ctx, msg) =>
      msg match {
        case ConvertToSeconds(duration, from) =>
          from ! ReplyLong(x = convertToSeconds(duration = duration))
          Behaviors.same
        case SplitDuration(duration, from) =>
          from ! ReplyLongLong(x = splitDuration(duration = duration))
          Behaviors.same
        case ComputeCallCost(splitDuration, from) =>
          from ! ReplyLong(x = computeCallCost(splitDuration = splitDuration))
          Behaviors.same
        case ComputeTotalCostPences(callsCost, maxCost, from) => 
          from ! ReplyLong(x = computeTotalCostHundredthPences(callsCost = callsCost, maxCost = maxCost))
          Behaviors.same
        case _ =>
          Behaviors.same
      }
    }

  private def addCall(state: State, number: String, duration: String): State = {
    val callCost = computeCallCost(splitDuration = splitDuration(duration = convertToSeconds(duration = duration)))

    val totalCostForNumber: Long = state.callsCost.get(number) match {
      case Some(x) =>
        callCost + x
      case None =>
        callCost
    }

    state.copy(
      callsCost = state.callsCost ++ Map(number -> totalCostForNumber), 
      maxCost = List(state.maxCost, totalCostForNumber).max)
  }

  private def convertToSeconds(duration: String): Long = {
    val x: Array[String] = duration.split(":").toArray
    x(0).toLong * 60 * 60 + x(1).toLong * 60 + x(2).toLong
  }

  private def splitDuration(duration: Long): (Long, Long) = 
    duration match {
      case duration if duration <= 3 * 60 =>
        (duration, 0)
      case _ =>
        (3 * 60, duration - 3 * 60)
    }

    private def computeCallCost(splitDuration: (Long, Long)): Long = {
      splitDuration._1 * 5 + splitDuration._2 * 3
    }

    private def computeTotalCostHundredthPences(callsCost: Map[String, Long], maxCost: Long): Long = {
      callsCost.values.sum - maxCost
    }
}
