package com.ricardomiranda.phone

import akka.actor.typed.ActorRef

sealed trait Message
case class AddCall(number: String, duration: String) extends Message
case class AskTotal(from: ActorRef[Message]) extends Message
case object AskTotals extends Message
case class NewLogLine(client: String, number: String, duration: String) extends Message
case object Working extends Message
case class ReplyTotalPounds(client: String, total: Double) extends Message
case object Terminate extends Message

// Testing messages
case class AskListOfClients(from: ActorRef[Message]) extends Message
case class ComputeCallCost(splitDuration: (Long, Long), from: ActorRef[Message]) extends Message
case class ComputeTotalCostPences(callsCost: Map[String, Long], maxCost: Long, from: ActorRef[Message]) extends Message
case class ConvertToSeconds(duration: String, from: ActorRef[Message]) extends Message
case class ReplyListOfString(xs: List[String]) extends Message
case class ReplyLong(x: Long) extends Message
case class ReplyLongLong(x: (Long, Long)) extends Message
case class ReplySetOfTuples(xs: Set[(String, Double)]) extends Message
case class SeeTotals(from: ActorRef[Message]) extends Message
case class SplitDuration(duration: Long, from: ActorRef[Message]) extends Message
case object Testing extends Message