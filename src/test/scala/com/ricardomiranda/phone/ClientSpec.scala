package com.ricardomiranda.phone

import akka.actor.typed._
import akka.actor.testkit.typed.scaladsl.{ActorTestKit, BehaviorTestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, FunSuite, Matchers, WordSpec}

class ClientSpec
  extends WordSpec
    with Matchers
    with BeforeAndAfterAll {

  val testKit = ActorTestKit()


  "A client" must {
    "be alive" in {
      val behaviorTestKit: BehaviorTestKit[Message] = BehaviorTestKit(new Client().rest)
      assert(behaviorTestKit.isAlive)
    }
  }

  "Ask for total from client with no calls" must {
    "return 0" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: (String, Double) = ("Client", 0.0)
      client ! Working
      client ! AskTotal(from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyTotalPounds(client = expected._1, total = expected._2))
    }
  }
 
  "Ask for total from client with a call of zero value" must {
    "return 0" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: (String, Double) = ("Client", 0.0)
      client ! Working
      client ! AddCall(number = "555-333-212", duration = "00:00:00")
      client ! AskTotal(from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyTotalPounds(client = expected._1, total = expected._2))
    }
  }
 
  "Ask for total from client with a sigle call" must {
    "return 0" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: (String, Double) = ("Client", 0.0)
      client ! Working
      client ! AddCall(number = "555-333-212", duration = "00:01:00")
      client ! AskTotal(from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyTotalPounds(client = expected._1, total = expected._2))
    }
  }
 
  "Ask for total from client with a sigle number in the call log" must {
    "return 0" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: (String, Double) = ("Client", 0.0)
      client ! Working
      client ! AddCall(number = "555-333-212", duration = "00:01:00")
      client ! AddCall(number = "555-333-212", duration = "00:01:00")
      client ! AddCall(number = "555-333-212", duration = "01:01:00")
      client ! AddCall(number = "555-333-212", duration = "00:01:01")
      client ! AddCall(number = "555-333-212", duration = "00:00:01")
      client ! AddCall(number = "555-333-212", duration = "00:01:00")
      client ! AskTotal(from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyTotalPounds(client = expected._1, total = expected._2))
    }
  }
 
  "Ask for total from client with a 2 numbers in the call log with zero length" must {
    "return 0" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: (String, Double) = ("Client", 0.0)
      client ! Working
      client ! AddCall(number = "555-333-212", duration = "00:00:00")
      client ! AddCall(number = "555-333-212", duration = "00:00:00")
      client ! AddCall(number = "555-333-212", duration = "00:00:00")
      client ! AddCall(number = "555-333-211", duration = "00:00:00")
      client ! AddCall(number = "555-333-211", duration = "00:00:00")
      client ! AddCall(number = "555-333-212", duration = "00:00:00")
      client ! AskTotal(from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyTotalPounds(client = expected._1, total = expected._2))
    }
  }
 
  "Ask for total from client with a 3 numbers" must {
    "return the cost of the 2 smaller" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: (String, Double) = ("Client", 0.0155)
      client ! Working
      client ! AddCall(number = "555-333-211", duration = "00:00:01")
      client ! AddCall(number = "555-333-211", duration = "00:00:10")
      client ! AddCall(number = "555-333-212", duration = "00:00:10")
      client ! AddCall(number = "555-333-212", duration = "00:00:10")
      client ! AddCall(number = "555-333-213", duration = "01:00:00")
      client ! AddCall(number = "555-333-213", duration = "10:00:00")
      client ! AskTotal(from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyTotalPounds(client = expected._1, total = expected._2))
    }
  }

  "Conversion of a zero length duration" must {
    "return 0" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Long = 0
      client ! Testing
      client ! ConvertToSeconds(duration = "00:00:00", from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLong(x = expected))
    }
  }

  "Conversion of a 1 second length duration" must {
    "return 1" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Long = 1
      client ! Testing
      client ! ConvertToSeconds(duration = "00:00:01", from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLong(x = expected))
    }
  }

  "Conversion of a 1 minute length duration" must {
    "return 60" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Long = 60
      client ! Testing
      client ! ConvertToSeconds(duration = "00:01:00", from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLong(x = expected))
    }
  }

  "Conversion of a 1 hour length duration" must {
    "return 3600" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Long = 3600
      client ! Testing
      client ! ConvertToSeconds(duration = "01:00:00", from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLong(x = expected))
    }
  }

  "Conversion of a 1 hour, 1 minute and 1 second length duration" must {
    "return 3661" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Long = 3661
      client ! Testing
      client ! ConvertToSeconds(duration = "01:01:01", from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLong(x = expected))
    }
  }

  "Splitting a phone call of zero length duration" must {
    "return (0, 0)" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: (Long, Long) = (0, 0)
      client ! Testing
      client ! SplitDuration(duration = 0, from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLongLong(x = expected))
    }
  }

  "Splitting a phone call of 1 second length duration" must {
    "return (1, 0)" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: (Long, Long) = (1, 0)
      client ! Testing
      client ! SplitDuration(duration = 1, from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLongLong(x = expected))
    }
  }

  "Splitting a phone call of 10 minutes length duration" must {
    "return (180, 420)" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: (Long, Long) = (3 * 60, 7 * 60)
      client ! Testing
      client ! SplitDuration(duration = 10 * 60, from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLongLong(x = expected))
    }
  }

  "Splitting a phone call of 3 minutes length duration" must {
    "return (180, 0)" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: (Long, Long) = (3 * 60, 0)
      client ! Testing
      client ! SplitDuration(duration = 3 * 60, from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLongLong(x = expected))
    }
  }

  "The cost of a phone call of zero length duration" must {
    "return 0" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Long = 0
      client ! Testing
      client ! ComputeCallCost(splitDuration = (0, 0), from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLong(x = expected))
    }
  }

  "The cost of a phone call of 1 second length duration" must {
    "return 5" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Long = 5
      client ! Testing
      client ! ComputeCallCost(splitDuration = (1, 0), from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLong(x = expected))
    }
  }

  "The cost of a phone call of 3 minutes length duration" must {
    "return 900" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Long = 3 * 60 * 5
      client ! Testing
      client ! ComputeCallCost(splitDuration = (3 * 60, 0), from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLong(x = expected))
    }
  }

  "The cost of a phone call of 4 minutes length duration" must {
    "return 1020" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Long = 3 * 60 * 5 + 60 * 3
      client ! Testing
      client ! ComputeCallCost(splitDuration = (3 * 60, 60), from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLong(x = expected))
    }
  }

  "The total cost of an empty list of calls" must {
    "return 0" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Long = 0
      client ! Testing
      client ! ComputeTotalCostPences(callsCost = Map(), maxCost = 0, from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLong(x = expected))
    }
  }

  "The total cost of a list of calls of zero" must {
    "return 0" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Long = 0
      client ! Testing
      client ! ComputeTotalCostPences(callsCost = Map("555-333-212" -> 0), maxCost = 0, from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLong(x = expected))
    }
  }

  "The total cost of a list of a single number" must {
    "return 0" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Long = 0
      client ! Testing
      client ! ComputeTotalCostPences(callsCost = Map("555-333-212" -> 10), maxCost = 10, from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLong(x = expected))
    }
  }

  "The total cost of a list of with 2 calls costs 10 and 4" must {
    "return 4" in {
      val client: ActorRef[Message] = testKit.spawn(new Client().rest, "Client")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Long = 4
      client ! Testing
      client ! ComputeTotalCostPences(
        callsCost = Map("555-333-212" -> 10, "555-663-111" -> 4), 
        maxCost = 10, 
        from = probe.ref)
      Thread.sleep(100)
      testKit.stop(client)

      probe.expectMessage(ReplyLong(x = expected))
    }
  }

  override def afterAll(): Unit = testKit.shutdownTestKit()
}
