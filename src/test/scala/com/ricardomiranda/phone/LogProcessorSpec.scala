package com.ricardomiranda.phone

import akka.NotUsed
import akka.actor.typed._
import akka.actor.testkit.typed.Effect.Spawned
import akka.actor.typed.scaladsl.Behaviors.Receive
import akka.actor.testkit.typed.scaladsl.{ActorTestKit, BehaviorTestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, GivenWhenThen, Matchers, WordSpec}

class LogProcessorSpec
  extends WordSpec
    with Matchers
    with BeforeAndAfterAll
    with GivenWhenThen {

  "A LogProcessor" when {
    "created" should {
      "create an ActorSystem" that {
        "has 1 dispatcher" in {
          Given("An empty system")
          When("no file is loaded")
          val given: BehaviorTestKit[NotUsed] = BehaviorTestKit(LogProcessor.root(("")))

          Then("1 actor should exist")
          val expected: Int = 1
          val actual: Int = given
            .retrieveAllEffects
            .filter(_.isInstanceOf[Spawned[Dispatcher]])
            .size

          actual shouldEqual expected
         }

        "and also" in {
          Given("An empty system")
          When("no file is loaded")
          val given: BehaviorTestKit[NotUsed] = BehaviorTestKit(LogProcessor.root(""))

          Then("the dispatcher is named Dispatcher")
          val expected = Set("Dispatcher")
          val actual: Set[String] = given
            .retrieveAllEffects
            .filter(_.isInstanceOf[Spawned[Dispatcher]])
            .map(_.asInstanceOf[Spawned[Dispatcher]].childName)
            .toSet

          actual shouldEqual expected
        }

        "and" in {
          Given("An empty system")
          When("an empty file is loaded")
          val given: BehaviorTestKit[NotUsed] = BehaviorTestKit(LogProcessor.root("src/test/resources/calls.0_lines.log"))

          Then("Dispatcher does nothing")
          val expected = Set("Dispatcher")
          val actual: Set[String] = given
            .retrieveAllEffects
            .filter(_.isInstanceOf[Spawned[Dispatcher]])
            .map(_.asInstanceOf[Spawned[Dispatcher]].childName)
            .toSet

          actual shouldEqual expected
        }
       }
     }
   }
}

class DipatcherSpec
  extends WordSpec
    with Matchers
    with BeforeAndAfterAll {

  val testKit = ActorTestKit()


  "A Dispatcher" must {
    "be alive" in {
      val behaviorTestKit: BehaviorTestKit[Message] = BehaviorTestKit(new Dispatcher().rest)
      assert(behaviorTestKit.isAlive)
    }
  }

  "A Dispatcher with no clients" must {
    "have an empty list of clients" in {
      val dispatcher: ActorRef[Message] = testKit.spawn(new Dispatcher().rest, "Dispatcher")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: List[String] = List()
      dispatcher ! Working
      dispatcher ! Testing
      dispatcher ! AskListOfClients(from = probe.ref)
      Thread.sleep(100)
      testKit.stop(dispatcher)

      probe.expectMessage(ReplyListOfString(xs = expected))
    }
  }

  "A Dispatcher with 1 client" must {
    "have a list of 1 client" in {
      val dispatcher: ActorRef[Message] = testKit.spawn(new Dispatcher().rest, "Dispatcher")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: List[String] = List("Client")
      dispatcher ! Working
      dispatcher ! NewLogLine(client = "Client", number = "555-334-789", duration = "00:01:59")
      dispatcher ! Testing
      dispatcher ! AskListOfClients(from = probe.ref)
      Thread.sleep(1000)
      testKit.stop(dispatcher)

      probe.expectMessage(ReplyListOfString(xs = expected))
    }
  }

  "A Dispatcher with 1 client and 2 log lines" must {
    "have a list of 1 client" in {
      val dispatcher: ActorRef[Message] = testKit.spawn(new Dispatcher().rest, "Dispatcher")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: List[String] = List("Client")
      dispatcher ! Working
      dispatcher ! NewLogLine(client = "Client", number = "555-334-789", duration = "00:01:59")
      dispatcher ! NewLogLine(client = "Client", number = "555-334-889", duration = "00:01:59")
      dispatcher ! Testing
      dispatcher ! AskListOfClients(from = probe.ref)
      Thread.sleep(1000)
      testKit.stop(dispatcher)

      probe.expectMessage(ReplyListOfString(xs = expected))
    }
  }

  "A Dispatcher with 3 clients" must {
    "have a list of 3 clients" in {
      val dispatcher: ActorRef[Message] = testKit.spawn(new Dispatcher().rest, "Dispatcher")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: List[String] = List("Client_01", "Client_02", "Client_03")
      dispatcher ! Working
      dispatcher ! NewLogLine(client = "Client_01", number = "555-334-789", duration = "00:01:59")
      dispatcher ! NewLogLine(client = "Client_02", number = "555-334-889", duration = "00:01:29")
      dispatcher ! NewLogLine(client = "Client_02", number = "555-334-789", duration = "00:41:59")
      dispatcher ! NewLogLine(client = "Client_03", number = "555-334-889", duration = "10:01:59")
      dispatcher ! NewLogLine(client = "Client_01", number = "555-334-709", duration = "00:11:59")
      dispatcher ! NewLogLine(client = "Client_03", number = "555-334-889", duration = "00:01:59")
      dispatcher ! Testing
      dispatcher ! AskListOfClients(from = probe.ref)
      Thread.sleep(1000)
      testKit.stop(dispatcher)

      probe.expectMessage(ReplyListOfString(xs = expected))
    }
  }

  "A Dispatcher with no clients" must {
    "recieve no total from clients" in {
      val dispatcher: ActorRef[Message] = testKit.spawn(new Dispatcher().rest, "Dispatcher")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Set[(String, Double)] = Set()
      dispatcher ! Working
      dispatcher ! Testing
      dispatcher ! AskTotals
      dispatcher ! SeeTotals(from = probe.ref)
      Thread.sleep(1000)
      testKit.stop(dispatcher)

      probe.expectMessage(ReplySetOfTuples(xs = expected))
    }
  }

  "A Dispatcher with 1 client with zero duration call" must {
    "recieve a zero account from clients" in {
      val dispatcher: ActorRef[Message] = testKit.spawn(new Dispatcher().rest, "Dispatcher")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Set[(String, Double)] = Set(("Client", 0.0))
      dispatcher ! Working
      dispatcher ! NewLogLine(client = "Client", number = "555-334-789", duration = "00:00:00")
      dispatcher ! Testing
      dispatcher ! AskTotals
      Thread.sleep(1000)
      dispatcher ! SeeTotals(from = probe.ref)
      Thread.sleep(1000)
      testKit.stop(dispatcher)

      probe.expectMessage(ReplySetOfTuples(xs = expected))
    }
  }

  "A Dispatcher with 1 client with 1 call" must {
    "recieve a zero account from clients" in {
      val dispatcher: ActorRef[Message] = testKit.spawn(new Dispatcher().rest, "Dispatcher")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Set[(String, Double)] = Set(("Client", 0.0))
      dispatcher ! Working
      dispatcher ! NewLogLine(client = "Client", number = "555-334-789", duration = "00:01:59")
      dispatcher ! Testing
      dispatcher ! AskTotals
      Thread.sleep(1000)
      dispatcher ! SeeTotals(from = probe.ref)
      Thread.sleep(1000)
      testKit.stop(dispatcher)

      probe.expectMessage(ReplySetOfTuples(xs = expected))
    }
  }

  "A Dispatcher with 1 client with 2 calls to the same number" must {
    "recieve a zero account from clients" in {
      val dispatcher: ActorRef[Message] = testKit.spawn(new Dispatcher().rest, "Dispatcher")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Set[(String, Double)] = Set(("Client", 0.0))
      dispatcher ! Working
      dispatcher ! NewLogLine(client = "Client", number = "555-334-789", duration = "00:01:59")
      dispatcher ! NewLogLine(client = "Client", number = "555-334-789", duration = "20:01:59")
      dispatcher ! Testing
      dispatcher ! AskTotals
      Thread.sleep(1000)
      dispatcher ! SeeTotals(from = probe.ref)
      Thread.sleep(1000)
      testKit.stop(dispatcher)

      probe.expectMessage(ReplySetOfTuples(xs = expected))
    }
  }

  "A Dispatcher with 2 clients with 1 call" must {
    "recieve 2 zero accounts from clients" in {
      val dispatcher: ActorRef[Message] = testKit.spawn(new Dispatcher().rest, "Dispatcher")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Set[(String, Double)] = Set(("Client_01", 0.0), ("Client_02", 0.0))
      dispatcher ! Working
      dispatcher ! NewLogLine(client = "Client_02", number = "555-334-789", duration = "00:01:59")
      dispatcher ! NewLogLine(client = "Client_01", number = "555-334-789", duration = "20:01:59")
      dispatcher ! Testing
      dispatcher ! AskTotals
      Thread.sleep(1000)
      dispatcher ! SeeTotals(from = probe.ref)
      Thread.sleep(1000)
      testKit.stop(dispatcher)

      probe.expectMessage(ReplySetOfTuples(xs = expected))
    }
  }

  "A Dispatcher with 3 clients with several calls" must {
    "recieve 3 accounts from clients" in {
      val dispatcher: ActorRef[Message] = testKit.spawn(new Dispatcher().rest, "Dispatcher")
      val probe: TestProbe[Message] = testKit.createTestProbe()

      val expected: Set[(String, Double)] = Set(("Client_01", 0.01), ("Client_02", 0.0005), ("Client_03", 0.0))
      dispatcher ! Working
      dispatcher ! NewLogLine(client = "Client_03", number = "555-334-789", duration = "00:01:59")
      dispatcher ! NewLogLine(client = "Client_02", number = "555-334-789", duration = "00:01:59")
      dispatcher ! NewLogLine(client = "Client_01", number = "555-334-789", duration = "00:01:59")
      dispatcher ! NewLogLine(client = "Client_02", number = "555-334-780", duration = "00:00:01")
      dispatcher ! NewLogLine(client = "Client_01", number = "555-334-489", duration = "00:00:20")
      dispatcher ! Testing
      dispatcher ! AskTotals
      Thread.sleep(1000)
      dispatcher ! SeeTotals(from = probe.ref)
      Thread.sleep(1000)
      testKit.stop(dispatcher)

      probe.expectMessage(ReplySetOfTuples(xs = expected))
    }
  }

  override def afterAll(): Unit = testKit.shutdownTestKit()
}