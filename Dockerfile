FROM openjdk:8-jre-alpine
RUN mkdir -p /opt/app/disco
WORKDIR /opt/app
COPY ./src/scripts/run_jar.sh ./target/scala-2.12/disco-company.jar ./
COPY ./src/main/resources/calls.log ./disco/
RUN ["chmod", "+x", "./run_jar.sh"]
ENTRYPOINT ["./run_jar.sh"]
