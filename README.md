# Phone Company

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Each day at The Phone Company a batch job puts all the customer calls for the previous day into a single log file of:

`'customer id','phone number called','call duration'`

For a customer the cost of a call up to and including 3 minutes in duration is charged at 0.05p/sec, any call over 3 minutes in duration the additional time is charged at 0.03p/sec. However, there is a promotion on and the calls made to the phone number with the greatest total cost is removed from the customer's bill.

## Task

Write a program that when run will parse the `calls.log` file and print out the total cost of calls for the day for each customer. You can use any libraries you wish to.

## Run program
Clone project to your machine.
Go to project folder.
Run instructions:

```bash
sbt assembly
docker build -f ./Dockerfile . -t disco
docker run -v ~/disco:/opt/app/disco -it disco
```

You will be asked what fot the input file.
Total cost of calls for each customer will appear on the console.
 Put all files in the docker shared folder:


```bash
./disco/
```

For instance copy input file to:


```bash
~/disco/calls.log
```

And interact with program like so:

```bash
What is the name of the log file?
>./disco/calls.log
```

Customers totals will apear on the screen.


## Test program
To test the program do:

```bash
sbt test
```

### Comments on tests
I left tests that use test messages. When passing to production this system this messages and corresponding tests could be deleated. Unit tests can be restricted topublic methods so that implementation details are not revealed, only the observed behaviour.


### Example log file:

```bash
A 555-333-212 00:02:03
A 555-433-242 00:06:41
```

## Requisites
You must have docker in your machine.

## Authors
*   [Ricardo Miranda](https://github.com/ricardomiranda)

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
